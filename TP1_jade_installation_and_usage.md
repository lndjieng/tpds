# Tutoriel sur JADE

## Qu'est-ce que JADE ?

JADE (Java Agent DEvelopment Framework) est un framework open-source en Java pour le développement d'applications multi-agents. Il fournit une plate-forme permettant de créer, déployer et gérer des systèmes multi-agents. JADE offre un ensemble de fonctionnalités pour la communication entre agents, la gestion du cycle de vie des agents et la coordination des activités des agents.

## Concepts clés de JADE :

- **Agents** :
  - Les agents sont des entités autonomes qui peuvent percevoir leur environnement, prendre des décisions et agir en conséquence.
  - Chaque agent possède son propre thread d'exécution et communique avec d'autres agents via des messages.

- **Conteneurs** :
  - Les conteneurs sont des environnements d'exécution pour les agents.
  - Ils fournissent les ressources nécessaires à l'exécution des agents, tels que la gestion des threads et la communication entre agents.
  - Plusieurs conteneurs peuvent être exécutés sur une même machine ou sur des machines distantes, permettant ainsi de distribuer les agents sur un réseau.

- **Plateforme** :
  - La plateforme est l'ensemble des conteneurs qui interagissent pour former un système multi-agents.
  - Elle fournit un environnement dans lequel les agents peuvent être créés, déployés, et exécutés.

## Comportements dans JADE

Les comportements dans JADE (Java Agent DEvelopment Framework) sont des éléments fondamentaux qui définissent le fonctionnement d'un agent. Un comportement représente une tâche ou une action que l'agent est capable d'exécuter de manière autonome.

### Types de comportements :

1. **Comportements simples** :
   - **Behaviour** : Comportement de base dans JADE, utilisé pour une variété de tâches.
   - **OneShotBehaviour** : Exécuté une seule fois.

2. **Comportements cycliques** :
   - **CyclicBehaviour** : Exécuté en boucle tant que l'agent est actif.
   - **TickerBehaviour** : Exécuté périodiquement à intervalles spécifiés.

3. **Comportements réactifs** :
   - **WakerBehaviour** : Programmation d'une action après un délai spécifique.
   - **ParallelBehaviour** : Exécution de plusieurs comportements en parallèle.

4. **Comportements composés** :
   - **SequentialBehaviour** : Chaînage séquentiel de plusieurs comportements.
   - **FSMBehaviour** : Modélisation d'automates à états finis pour des comportements hiérarchiques.

Chaque type de comportement offre ses propres caractéristiques et avantages, permettant aux développeurs d'agents de choisir celui qui convient le mieux à leurs besoins spécifiques.

## Installation sur Windows

### Étapes pour installer JADE sur Windows :

1. **Téléchargement de JADE** :
   Rendez-vous sur le site officiel de JADE à l'adresse [http://jade.tilab.com/](http://jade.tilab.com/) et téléchargez la dernière version de JADE.

2. **Extraction des fichiers** :
   Une fois le téléchargement terminé, extrayez les fichiers de l'archive téléchargée dans un répertoire de votre choix sur votre système.

3. **Configuration de l'environnement** :
   Ajoutez le chemin du dossier `lib` de JADE à la variable d'environnement `CLASSPATH` de votre système. Cela permettra à Java de trouver les classes JADE lors de l'exécution.

## Installation sur Linux

### Étapes pour installer JADE sur Linux :

1. **Téléchargement de JADE** :
   Rendez-vous sur le site officiel de JADE à l'adresse [http://jade.tilab.com/](http://jade.tilab.com/) et téléchargez la dernière version de JADE.

2. **Extraction des fichiers** :
   Une fois le téléchargement terminé, ouvrez un terminal et utilisez la commande `tar -xvf nom_du_fichier.tar.gz` pour extraire les fichiers de l'archive téléchargée dans un répertoire de votre choix sur votre système.

3. **Configuration de l'environnement** :
   Ajoutez le chemin du dossier `lib` de JADE à la variable d'environnement `CLASSPATH` de votre système en éditant votre fichier `~/.bashrc` et en y ajoutant la ligne suivante :

```sh
export CLASSPATH=$CLASSPATH:/chemin/vers/lib/jade.jar
```


4. **Recharger le fichier de configuration** :
Après avoir enregistré les modifications apportées à votre fichier `~/.bashrc`, rechargez-le en exécutant la commande `source ~/.bashrc` dans votre terminal.

## Utilisation de JADE

### Lancement de JADE en ligne de commande :
Pour lancer la plateforme JADE en ligne de commande, utilisez la commande suivante :

```sh
java jade.Boot -gui
```
Cette commande démarre la plateforme JADE avec interface graphique.

### Création et exécution d'un agent en ligne de commande :

1. Créez une classe Java pour votre agent en étendant `jade.core.Agent`.
2. Implémentez la méthode `setup()` pour initialiser votre agent et définir son comportement.
3. Compilez votre classe Java avec la commande `javac VotreAgent.java`.
4. Pour lancer votre agent, utilisez la commande suivante :

```sh
java jade.Boot -gui -host localhost -agents MonAgent:VotreAgent
```
Remplacez `MonAgent` par le nom que vous souhaitez donner à votre agent et `VotreAgent` par le nom de votre classe Java.

### Utilisation de JADE à partir du code Java

#### Création de plateforme, conteneurs et agents
Voici un exemple de code Java utilisant JADE pour démarrer une plateforme, créer de nouveaux conteneurs et lancer de nouveaux agents dans ces conteneurs :

```java
import jade.core.Profile;
import jade.core.ProfileImpl;
import jade.wrapper.AgentContainer;
import jade.wrapper.AgentController;
import jade.core.Runtime;

public class Main {

    public static void main(String[] args) {
        // Initialiser JADE
        Runtime rt = Runtime.instance();
        Profile p = new ProfileImpl();
        p.setParameter(Profile.MAIN_HOST, "localhost");
        p.setParameter(Profile.GUI, "true"); // Activer l'interface graphique JADE
        AgentContainer mainContainer = rt.createMainContainer(p);

        try {
            // Créer un nouveau conteneur
            Profile containerProfile = new ProfileImpl();
            containerProfile.setParameter(Profile.MAIN_HOST, "localhost");
            AgentContainer container = rt.createAgentContainer(containerProfile);

            // Créer et lancer un nouvel agent dans le conteneur
            AgentController agentController = container.createNewAgent("Agent1", "YourAgentClassName", new Object[]{});
            agentController.start();

            // Créer et lancer un autre agent dans le même conteneur
            agentController = container.createNewAgent("Agent2", "YourAnotherAgentClassName", new Object[]{});
            agentController.start();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
```
#### Communication entre deux agents :

1. Pour faire communiquer deux agents, vous devez spécifier l'adresse (nom) de l'agent destinataire dans le message envoyé.
2. Dans la méthode `setup()` de votre premier agent, vous pouvez envoyer un message à un autre agent en utilisant la méthode `send()` de l'objet `Agent` :

```java
import jade.core.Agent;
import jade.core.AID;
import jade.core.behaviours.OneShotBehaviour;
import jade.lang.acl.ACLMessage;

public class SendingAgent extends Agent {
    @Override
    protected void setup() {
        // Créer un comportement pour envoyer le message une fois que l'agent est prêt
        addBehaviour(new OneShotBehaviour() {
            public void action() {
                // Créer un message ACL
                ACLMessage message = new ACLMessage(ACLMessage.INFORM);
                // Ajouter le contenu du message
                message.setContent("Salut, voici un message de la part de l'agent d'envoi!");
                // Ajouter le destinataire du message
                AID receiver = new AID("NomDeVotreAgentReceveur", AID.ISLOCALNAME);
                message.addReceiver(receiver);
                // Envoyer le message
                send(message);
            }
        });
    }
}
```
3. Pour qu'un agent reçoive les messages vous pouvez utiliser:

```java
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

public class VotreAgent extends Agent {
    @Override
    protected void setup() {
        // Ajoute un comportement pour recevoir des messages
        addBehaviour(new CyclicBehaviour(this) {
            public void action() {
                // Définit le modèle de message à recevoir (INFORM)
                MessageTemplate mt = MessageTemplate.MatchPerformative(ACLMessage.INFORM);
                // Reçoit un message correspondant au modèle
                ACLMessage msg = receive(mt);
                if (msg != null) {
                    // Traitement du message reçu
                    String contenu = msg.getContent();
                    System.out.println("Agent " + myAgent.getLocalName() + " a reçu le message : " + contenu);
                }
                else {
                    // Si aucun message correspondant n'est reçu, le comportement est bloqué jusqu'à la réception d'un tel message
                    block();
                }
            }
        });
    }
}
```

