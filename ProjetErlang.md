# Projet : Transformation d'un graphe complet de noeuds en arbre binaire en Erlang

## Objectif du projet

L'objectif de ce projet est de développer un programme en Erlang capable de transformer une topologie de graphe complet en une topologie d'arbre binaire, ensuite d'appliquer quelques concepts de base des systèmes distribués sur cet arbre. Un arbre binaire étant une structure hiérarchique de donnée où chaque noeud a au plus deux enfants.

## I. Consignes de restitution du projet

### I.1. Code
Le code doit être :
- correctement commenté pour expliquer les rôles des différentes fonctions écrites.
- accompagné d'un fichier README (qui ne doit pas être très long) décrivant comment l'exécuter et l'utiliser.

**NB:** Le respect de ces consignes vaut ***1 pt***.

### I.2. Documentation
La documentation doit :
- tenir sur 8 pages au plus.
- expliquer en détail le fonctionnement du programme.
- inclure des exemples d'utilisation du programme avec des entrées et des sorties.
- expliquer la méthode utilisée pour transformer le graphe complet en arbre binaire.
- expliquer comment sont gérées les éventuelles erreurs ou situations exceptionnelles.

**NB:** Le respect de ces consignes vaut ***5 pts***.

### I.3. Restitution du projet
- Le code et la documentation doivent être archivés dans un fichier `nom_de_l_etudiant.tar.gz`.
- Cette archive doit être envoyée par courriel à [Lucien NGALE](mailto:lucien.ndjie@ens-lyon.fr).
- La date limite de restitution est fixée au `05 avril 2024` à `23h59`.
- **Avant la deadline si vous avez la moindre question n'hésitez pas à envoyer un mail à [Lucien NGALE](mailto:lucien.ndjie@ens-lyon.fr)**.

**NB:** Le respect de ces consignes vaut ***1 pt***.

## II. Tâches à réaliser
Noter que l'exécution se fera sur un unique nœud Erlang. Ce que nous appellerons donc nœud du réseau, ici sera un processus créé par `spawn/3`.

### II.1. Transformation du graphe complet en arbre binaire ***(5 pts)***
1. Écrire une fonction qui crée un graphe complet de N processus ***(1.5 pt)***:
    - Chaque processus affiche son PID et un nombre généré aléatoirement, puis l'envoie à tous ses voisins ;
    - Chaque processus affiche le nombre reçu, le PID de l'émetteur ainsi que son propre PID.
2. Implémenter un algorithme pour transformer le graphe complet en un arbre binaire ***(3.5 pts)***:
    - Chaque processus va redéfinir la liste de ses voisins qui sera constituée uniquement de ses enfants et qui sera vide s'il s'agit d'une feuille de l'arbre ;
    - Il va ensuite afficher cette liste ainsi que le PID de son parent;
    - Hypothèses:
        - Le graphe complet est maintenu tant que l'arbre binaire n'est pas complètement construit;
        - Il n'y a pas de panne.

### II.2. Diffusion de messages ***(3 pts)***
Mettre en place un protocole simple pour diffuser un message dans l'arbre binaire et permettre au diffuseur de savoir que tous les nœuds l'ont reçu, pour trois cas de figure :

1. Le processus racine diffuse le message ;
2. Un processus feuille diffuse le message ;
3. Un processus intermédiaire diffuse le message.

Noter que:
- Chaque processus excepté le diffuseur doit afficher le message diffusé quand il le reçoit;
- Le diffuseur doit afficher "Je suis certain que tout le monde a reçu mon message :-)" quand il saura que tous les processus ont reçu son message;
- Il n'y a pas de panne.

### II.3. Tolérance aux pannes ***(5 pts)***
1. Mettre en place un mécanisme de détection des pannes pour les nœuds de l'arbre. Tous les noeuds du système doivent afficher les PID des noeuds qui sont suspectés de panne. ***(2.5x2 pts)***
2. ***(facultatif)*** Mettre en place un algorithme qui reconstitut un arbre binaire en cas de détection de pannes. ***(1 pt)***

### II.4. Exclusion mutuelle (facultatif) ***(1 pt)***
Supposez que chaque processus de votre arbre binaire soit un employé d'un petit laboratoire qui ne dispose que d'une seule imprimante. Mettez en place un algorithme d'exclusion mutuelle qui permettra de s'assurer que chaque employé ait la possibilité d'imprimer ses documents de manière ordonnée afin d'éviter les conflits.

