# Tutoriel Gradle + JADE

## Gradle
Gradle est un système de gestion de construction (build system) open source conçu pour automatiser le processus de construction, de test et de déploiement des projets logiciels. Gradle utilise une syntaxe déclarative basée sur le langage de programmation [Groovy](https://groovy-lang.org/documentation.html) ou [Kotlin](https://developer.android.com/kotlin/learn?hl=fr) pour décrire les dépendances du projet, les tâches de construction et les configurations. Il offre une grande flexibilité et une configuration basée sur des plugins, ce qui permet aux développeurs de personnaliser et d'étendre facilement le processus de construction en fonction des besoins spécifiques de leur projet. Gradle est utilisé dans de nombreux projets et cadres de développement, y compris Android, Java, Kotlin, Groovy, Scala et bien d'autres.

### Installation de Gradle
Vous pouvez regarder la section [Installation manuelle de Gradle, ici](https://gradle.org/install/).

### Créer et démarrer un projet Gradle
* Créez un nouveau dossier où vous voulez que votre projet soit situé.
* Dans le terminal ou l'invite de commande, naviguez vers le dossier de votre projet et exécutez la commande suivante pour initialiser un projet Gradle :
    ```sh
    gradle init
    ```
* Vous aurez alors une série de questions qui apparaissent successivement:
    * Aux questions de savoir quel est le type de projet, quel est le langage de votre application et quel est le langage de script, choisissez respectivement `application`, `Java` et `Groovy`. 
    * Pour toutes les autres questions, appuyez sur la touche `entrée` du clavier pour valider le choix par défaut.
* Vous pouvez ensuite lancer le programme de base généré qui affiche un `Hello World !` :
    ```sh
    gradle run
    ```
    Ce qui s'est passé ? 
    * Gradle a considéré que le dossier source est `VOTRE_PROJET/app/` du fait de ces lignes spécifiées dans le fichier `VOTRE_PROJET/settings.gradle` :
        ```groovy
        rootProject.name = 'VOTRE_PROJET'
        include('app')
        ```
    Vous pouvez renommer `app` si vous le souhaitez et mettre le nom que vous avez remplacé comme paramètre à la fonction `include()` du fichier `settings.gradle`.
    * Gradle a lancé la classe `app/src/main/java/org/example/App.java` du projet car c'est ce qui a été précisé dans le fichier `app/build.gradle` :
        ```groovy
        application {
            // Définir la classe principale pour l'application.
            mainClass = 'org.example.App'
        }
        ```
    Vous pouvez créer votre package (dans `VOTRE_APP/src/main/java/`) dans lequel vous créez une classe (`VOTRE_CLASSE.java`) et vous remplacez la valeur de l'attribut `mainClass` par `VOTRE_PACKAGE.VOTRE_CLASSE`, pour lancer votre propre code.
## Comment ça se passe avec Jade ?
* Créez un dossier `libs` dans `VOTRE_APP`, puis copiez-y le jar de Jade que vous avez téléchargé.
* Rajoutez ensuite les lignes suivantes dans `dependencies` dans le fichier `VOTRE_APP/build.gradle` :
    ```groovy
    implementation files('libs/jade-x.x.x.jar')
    implementation group: 'commons-codec', name: 'commons-codec', version: '1.15'
    implementation 'org.slf4j:slf4j-api:2.0.6'
    implementation 'org.slf4j:slf4j-simple:2.0.6'
    ```
## Travail à faire
1. Écrire une classe Java dans votre package qui :

    a. crée une plateforme JADE

    b. y crée un conteneur principal ainsi que deux autres conteneurs (que vous nommerez à votre convenance)

    c. dans chaque conteneur, vous créez un agent que vous nommerez à votre convenance

2. Écrire les classes Java dont dépendent les agents, sachant qu'ils s'envoient des messages (des nombres qu'ils choisissent aléatoirement) et affichent les messages reçus, en faisant un Ping-Pong.

3. À partir de `gradle`, lancez votre programme.

4. Faites le même exercice pour les topologies suivantes (avec N agents créés) : ring et star.

    **NB** : Il est possible de passer *N* en paramètre dans gradle en faisant :
    ```sh
    gradle run --args="Valeur_de_N"
    ```
    `Valeur_de_N` va alors correspondre au premier élément (`args[0]`) du tableau `args` passé en paramètre de la fonction `main()` de la classe appelée par gradle.
